/**
 * @desc Retrieves data from given url and
 * reformats to json
 * @param {String} url -
 * @returns {Promise}
 */
export const Fetch = (url) => {
  return new Promise((resolve, reject) => {
    fetch(url)
      .then(res => res.json())
      .then(data => resolve(data))
      .catch(err => reject(err));
  });
}
