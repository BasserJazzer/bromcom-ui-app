import { LitElement, html } from 'lit-element';
import { repeat } from 'lit-html/directives/repeat.js';
import { Constants } from '../../constants/constants.js';
import { Fetch } from '../../lib/helpers';
import '../../@components/dropdown/dropdown.js';
import '../../@components/button/button.js';
import '../../@components/image/image.js';

class BuiPageMyFavorites extends LitElement {
  static get properties() {
    return {
      items: {type: Array}
    };
  }

  constructor() {
    super();
    this.items = [];

    this.getFavorites();
  }

  render() {
    return html`
      <div class="container">
        ${this.items ? this.items.length != 0
          ? html`
            <div class="items">
              ${repeat(this.items, (item, index) => html`
                <bui-image .data="${item}"></bui-image>
              `)}
            </div>
          `
          : html`
            <div class="no-result">
              No favorite image found!
            </div>
          ` : ``}
      </div>
      <link rel="stylesheet" href="css/pages/my-favorites/style.css" />
    `;
  }

  /**
   * @desc
   *
   * @param {Event} event -
   * @returns {Void}
   */
  getFavorites() {
    const items = JSON.parse(localStorage.getItem('like-items'));

    if (items)
      for (let item in items) this.items.push(items[item]);
    this.requestUpdate();
  }
}
customElements.define('bui-page-my-favorites', BuiPageMyFavorites);
