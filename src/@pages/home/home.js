import { LitElement, html } from 'lit-element';
import { repeat } from 'lit-html/directives/repeat.js';
import { Constants } from '../../constants/constants.js';
import { Fetch } from '../../lib/helpers';
import '../../@components/dropdown/dropdown.js';
import '../../@components/button/button.js';
import '../../@components/image/image.js';

class BuiPageHome extends LitElement {
  static get properties() {
    return {
      selectedCategory: {type: String},
      items: {type: Array}
    };
  }

  render() {
    return html`
      <div class="container">
        <div class="actions">
          <bui-dropdown @selected="${(e) => this.categorySelected(e.detail.category)}"></bui-dropdown>
          <bui-button
            @click="${(e) => this.getData()}"
            .active="${this.items ? 'active' : '' }"
          >Show</bui-button>
        </div>
        ${this.items ? this.items.length != 0
          ? html`
            <div class="results">
              ${repeat(this.items, (item, index) => html`
                <bui-image .data="${item}"></bui-image>
              `)}
            </div>
          `
          : html`
            <div class="no-result">
              Sorry, we couldn't find any matches. <br>
              Please try another category
            </div>
          ` : ``}
      </div>
      <link rel="stylesheet" href="css/pages/home/style.css" />
    `;
  }

  /**
   * @desc
   *
   * @param {String} category -
   * @returns {Void}
   */
  categorySelected(category) {
    this.selectedCategory = category
  }

  /**
   * @desc
   *
   * @param {Event} event -
   * @returns {Void}
   */
  getData(event) {
    if(!this.selectedCategory)
      return;

    const url = `${Constants.PIXABAY_API}&category=education${this.selectedCategory != 'education' ? '&q=' + this.selectedCategory.replace('-', '+') : ''}`;

    // Get data from query
    // #
    Fetch(url).then((data) => {
      const list = data.hits;
      list.map((item) => item.liked = false);

      this.items = list;
    });
  }
}
customElements.define('bui-page-home', BuiPageHome);
