import { LitElement, html, css } from 'lit-element';
import { repeat } from 'lit-html/directives/repeat.js';
import './@components/toast/toast.js';
import './@components/header/header.js';
import './@pages/home/home.js'
import './@pages/my-favorites/my-favorites.js'

class BuiApp extends LitElement {
  static get properties() {
    return {
      toasts: {type: Array},
      page: {type: String}
    }
  }

  static get styles() {
    return css`
      .notifications {
        position: fixed;
        bottom: 0;
        left: 0;
      }
    `;
  }

  constructor() {
    super();

    this.page = 'home';
    this.toasts = [];

    /**
     * [ Event: notify --> window ]
     */
    window.addEventListener('notify', (e) => {
      this.toasts.push({
        removed: e.detail.removed,
        id: (Math.random() * Math.pow(36, 4) | 0).toString(36)
      })
      this.requestUpdate();
    })

    /**
     * [ Event: notify --> window ]
     */
    window.addEventListener('changePage', (e) => {
      this.page = e.detail.page;
      this.requestUpdate();
    })
  }

  render() {
    return html`
      <bui-header
        @navigate="${(e) => this.navigate()}">
        ${this.page == 'home' ? 'My Favorites' : 'Home'}
      </bui-header>

      ${this.page == 'home'
        ? html`
          <bui-page-home></bui-page-home>
        `
        : html`
          <bui-page-my-favorites></bui-page-my-favorites>
        `
      }
      <div class="notifications">
        ${repeat(this.toasts, (item, index) => html`
          <bui-toast
            .type="${item.removed ? 'danger' : 'success'}"
            .id="${item.id}"
            @remove="${(e) => this.toastRemove(e.detail.id)}">
          </bui-toast>
        `)}
      </div>
    `;
  }

  /**
   * @desc
   *
   * @param {Number} index
   * @returns {Void}
   */
  toastRemove(id) {
    let index = this.toasts.map((x) => x.id).indexOf(id)

    this.toasts.splice(index, 1);
    console.log(this.toasts);
    this.requestUpdate();
  }

  /**
   * @desc
   *
   * @returns {Void}
   */
  navigate() {
    this.page == 'home'
      ? this.page = 'my-favorites'
      : this.page = 'home'
    ;
  }
}
customElements.define('bui-app', BuiApp);
