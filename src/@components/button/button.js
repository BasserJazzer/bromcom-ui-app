import {LitElement, html} from 'lit-element';

class BuiButton extends LitElement {
  static get properties() {
    return {
      active: {type: Boolean}
    }
  }

  render() {
    return html`
      <button class="button${this.active ? ' active' : ''}">
        <slot></slot>
      </button>
      <link rel="stylesheet" href="css/components/button/style.css" />
    `;
  }
}
customElements.define('bui-button', BuiButton);
