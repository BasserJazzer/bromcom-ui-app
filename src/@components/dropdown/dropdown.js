import { LitElement, html } from 'lit-element';
import { repeat } from 'lit-html/directives/repeat.js';

class BuiDropdown extends LitElement {
  static get properties() {
    return {
      categories: {type: Object},
      selected: {type: String}
    };
  }

  constructor() {
    super();
    this.selected = 'Select Category';
    this.categories = {
      'education': 'Education',
      'online-learning': 'Online Learning',
      'online-exam': 'Online Exam'
    };

    /**
     * [ Event: click --> document ]
     */
    document.addEventListener( 'click', (e) => {
      // Close dropdown when clicked anywhere
      // except itself
      if(e.path[0].nodeName.toString().toLowerCase() != 'input')
        this.toggle(false, false);
    })
  }

  render() {
    return html`
      <div class="dropdown">
        <input type="text" value="${this.selected}" readonly @click="${this.toggle}" />
        <ul>
          ${repeat(Object.keys(this.categories), (category, index) => html`
            <li @click="${() => this.itemClick(category)}">
              ${this.categories[category]}
            </li>
          `)}
        </ul>
      </div>
      <link rel="stylesheet" href="css/components/dropdown/style.css" />
    `;
  }

  /**
   * @desc
   *
   * @param {Event} e -
   * @param {Boolean} toggle -
   * @returns {Void}
   */
  toggle(e, toggle) {
    this.shadowRoot.querySelector('.dropdown').classList.toggle('active', toggle);
  }

  /**
   * @desc
   *
   * @param {String} category -
   * @returns {Void}
   */
  itemClick(category) {
    this.selected = this.categories[category];

    // Trigger selected event
    // #
    this.dispatchEvent(new CustomEvent('selected', {
      detail: { category: category }
    }));

    // Close dropdown
    // #
    this.toggle(false, false);
  }
}

customElements.define('bui-dropdown', BuiDropdown);
