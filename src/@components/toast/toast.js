import {LitElement, html} from 'lit-element';

class BuiToast extends LitElement {
  static get properties() {
    return {
      type: {type: Boolean},
      id: {type: String}
    }
  }

  constructor() {
    super();

    setTimeout(() => {
      this.remove();
    }, 5000);
  }

  render() {
    return html`
      <div class="toast ${this.type} shown">
        <span class="close" @click="${this.remove}">
          <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill="black" fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
          </svg>
        </span>
        ${this.type == 'success'
          ? 'The photo you like has been saved'
          : 'The photo deleted from My Favorites'
        }
      </div>
      <link rel="stylesheet" href="css/components/toast/style.css" />
    `;
  }

  /**
   * @desc
   *
   * @returns {Void}
   */
  remove() {
    // Trigger selected event
    // #
    this.dispatchEvent(new CustomEvent('remove', {
      detail: {
        id: this.id
      }
    }));
  }
}
customElements.define('bui-toast', BuiToast);
