import {LitElement, html} from 'lit-element';

class BuiHeader extends LitElement {
  render() {
    return html`
      <div class="header">
        <a @click="${this.handleClick}"><slot></slot></a>
      </div>
      <link rel="stylesheet" href="css/components/header/style.css" />
    `;
  }

  handleClick() {

    // Trigger navigate event
    // #
    this.dispatchEvent(new CustomEvent('navigate', {}));
  }
}
customElements.define('bui-header', BuiHeader);
